XHSocial
========

类似Vine的登录界面，后续加入更多组件，比如：获取好友列表、赞、个人资料等功能（注意：附带后台服务器PHP语言写的代码），希望能帮到你们


如下图:


![image](https://github.com/JackTeam/XHSocial/raw/master/Screenshots/login.png)



## License

中文:      XHSocial 是在MIT协议下使用的，可以在LICENSE文件里面找到相关的使用协议信息.

English:   XHSocial is acailable under the MIT license, see the LICENSE file for more information.
